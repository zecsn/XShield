-- This file is part of XShield.

-- XShield is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- XShield is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with XShield.  If not, see <http://www.gnu.org/licenses/>.

-- Copyright 2019-2020 Zecsn Technologies (SMC-Pvt) Ltd

function before(hook, param)
    param:setArgument(0, 0) -- latitude
    param:setArgument(1, 0) -- longitude
    param:setArgument(2, 0.1) -- radius
    return true
end
