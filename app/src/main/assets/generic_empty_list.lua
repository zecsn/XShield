-- This file is part of XShield.

-- XShield is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- XShield is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with XShield.  If not, see <http://www.gnu.org/licenses/>.

-- Copyright 2019-2020 Zecsn Technologies (SMC-Pvt) Ltd

function after(hook, param)
    local list = param:getResult()
    if list == nil then
        return false
    end
    local array = luajava.bindClass('java.lang.reflect.Array')
    if array:getLength(list:toArray()) == 0 then
        return false
    end

    local fake = luajava.newInstance('java.util.ArrayList')
    param:setResult(fake)
    return true
end
