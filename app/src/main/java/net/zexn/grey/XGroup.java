/*
    This file is part of XShield.

    XShield is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    XShield is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with XShield.  If not, see <http://www.gnu.org/licenses/>.

    Copyright 2019-2020 Zecsn Technologies (SMC-Pvt) Ltd
 */

package net.zexn.grey;

class XGroup {
    String name;
    String title;

    @Override
    public String toString() {
        return title;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof XGroup))
            return false;
        XGroup other = (XGroup) obj;
        return this.name.equals(other.name);
    }
}
